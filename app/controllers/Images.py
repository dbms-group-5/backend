from __future__ import absolute_import

import base64

import io
from flask import Blueprint, jsonify, request, make_response, send_file
from flask_login import login_required

from configs.Errors import error_code

from app.models import db, o2d
from app.models.Images import Images

image_blueprint = Blueprint('Images', __name__)


@image_blueprint.route("/", methods=['POST'])
@login_required
def create_image():
    data = request.form

    new_image = Images(
        image=data['file'],
        user_id=data['user_id'],
        post_id=data['post_id']
    )

    try:
        db.session.add(new_image)
        db.session.commit()
        return jsonify({
            'message': 'Uploaded image',
            'error_code': error_code.success
        })
    except Exception as e:
        db.session.rollback()
        return jsonify({
            'message': str(e),
            'error_code': error_code.failed
        })


@image_blueprint.route("/", methods=['GET'])
@login_required
def get_all_image():
    images = Images.query.all()
    result = []

    for im in images:
        result.append(o2d(im))

    response = {
        'data': result
    }
    response['error_code'] = error_code.success
    return jsonify(response)


@image_blueprint.route("/<int:id>", methods=['GET'])
@login_required
def get_image(id):
    image = Images.query.get(id)

    if not image:
        return jsonify({
            'message': 'Not found',
            'error_code': error_code.not_found
        })

    content = image.image
    return send_file(io.BytesIO(content), as_attachment=True, mimetype="image/jpeg", attachment_filename='{}_{}.jpg'.format("image", id))


@image_blueprint.route("/<int:id>", methods=['PUT'])
@login_required
def update_image(id):
    data = request.form

    image = Images.query.get(id)

    if not image:
        return jsonify({
            'message': 'Not found',
            'error_code': error_code.not_found
        })

    image.image = data['file']

    try:
        db.session.commit()
        return jsonify({
            'message': 'Updated image {}'.format(id),
            'error_code': error_code.success
        })
    except Exception as e:
        db.session.rollback()
        return jsonify({
            'message': str(e),
            'error_code': error_code.failed
        })


@image_blueprint.route("/<int:id>", methods=['DELETE'])
@login_required
def delete_image(id):
    image = Images.query.get(id)

    if not image:
        return jsonify({
            'message': 'Not found',
            'error_code': error_code.not_found
        })

    try:
        db.session.delete(image)
        db.session.commit()
        return jsonify({
            'message': 'Deleted image {}'.format(id),
            'error_code': error_code.success
        })
    except Exception as e:
        db.session.rollback()
        return jsonify({
            'message': str(e),
            'error_code': error_code.failed
        })
