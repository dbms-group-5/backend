from __future__ import absolute_import

import io
from flask import Blueprint, jsonify, request, send_file
from flask_login import login_user, logout_user, login_required, current_user

from app.models.Users import Users

from app.models import db, o2d

from configs.Errors import error_code

from utils.utils import parseDate, parseDateTime

from werkzeug.security import generate_password_hash, check_password_hash

users_blueprint = Blueprint('Users', __name__)


@users_blueprint.route("/", methods=['GET'])
@login_required
def get_all_user():
    users = Users.query.all()
    result = []
    for user in users:
        u = o2d(user)
        del u['password']
        result.append(u)
    response = {
        'data': result
    }
    response["error_code"] = error_code.success
    return jsonify(response)


@users_blueprint.route("/image/<int:id>", methods=['GET'])
@login_required
def get_image_of_user(id):
    user = Users.query.get(id)

    if not user:
        return jsonify({
            'message': 'Not found',
            'error_code': error_code.not_found
        })

    content = user.image.image
    return send_file(io.BytesIO(content), as_attachment=True, mimetype="image/jpeg", attachment_filename='{}_{}.jpg'.format("user_image", id))


@users_blueprint.route("/<int:id>", methods=['GET'])
@login_required
def get_user(id):
    user = Users.query.get(id)
    if not user:
        response = {
            'message': "User not found",
            'error_code': error_code.not_found
        }
    else:
        response = o2d(user)
        del response["password"]
        response["error_code"] = error_code.success
    return jsonify(response)


@users_blueprint.route('/register', methods=['POST'])
def create_user():
    data = request.json
    data['password'] = generate_password_hash(data['password'])

    new_user = Users(
        username=data['username'],
        password=data['password'],
        name=data['name'],
        birthday=parseDate(data['birthday']),
        phone=data['phone'],
        email=data['email'],
        gender=data['gender'],
        role_id=data['role_id']
    )

    try:
        db.session.add(new_user)
        db.session.commit()
        response = o2d(new_user)
        response['error_code'] = error_code.success
    except Exception as e:
        db.session.rollback()
        response = {
            'message': str(e),
            'error_code': error_code.failed
        }
    return jsonify(response)


@users_blueprint.route('/<int:id>', methods=['PUT'])
@login_required
def update_user_by_id(id):
    user = Users.query.get(id)

    if not user:
        return jsonify({
            'message': 'Not found',
            'error_code': error_code.not_found
        })

    data = request.json
    user.name = data['name']
    user.username = data['username']
    user.birthday = parseDate(data['birthday'])
    user.phone = data['phone']
    user.email = data['email']
    user.gender = data['gender']
    if 'role_id' in data and data['role_id']:
        user.role_id = data['role_id']

    try:
        db.session.commit()
        response = {
            'message': 'Updated user {}'.format(user.email),
            'error_code': error_code.success
        }
    except Exception as e:
        db.session.rollback()
        response = {
            'message': str(e),
            'error_code': error_code.failed
        }

    return jsonify(response)


@users_blueprint.route('/<int:id>', methods=['DELETE'])
def delete_user_by_id(id):
    user = Users.query.get(id)

    if not user:
        return jsonify({
            'message': 'Not found',
            'error_code': error_code.not_found
        })

    try:
        db.session.delete(user)
        db.session.commit()
        response = {
            'message': 'Deleted user {}'.format(user.email),
            'error_code': error_code.success
        }
    except Exception as e:
        db.session.rollback()
        response = {
            'message': str(e),
            'error_code': error_code.failed
        }

    return jsonify(response)


@users_blueprint.route("/is-authenticated", methods=['GET'])
def is_au_yet():
    if current_user.is_authenticated:
        return jsonify({
            'message': 'Authenticated',
            'error_code': error_code.success
        })
    return jsonify({
        'message': "Not authenticated",
        'error_code': error_code.failed
    })


@users_blueprint.route('/login', methods=['POST'])
def login():
    data = request.json

    if 'username' in data:
        user = Users.query.filter_by(username=data['username']).first()
    elif 'email' in data:
        user = Users.query.filter_by(email=data['email']).first()
    else:
        return jsonify({
            'message': 'Missing username or email',
            'error_code': error_code.missing_field
        })

    if not user:
        return jsonify({
            'message': 'Not found user',
            'error_code': error_code.not_found
        })

    if not check_password_hash(user.password, data['password']):
        return jsonify({
            'message': 'Wrong password',
            'error_code': error_code.failed
        })

    response = o2d(user)
    response['error_code'] = error_code.success
    login_user(user, remember=True)
    return jsonify(response)


@users_blueprint.route('/logout')
@login_required
def logout():
    try:
        logout_user()
        response = {
            "message": "Successfully logged out",
            "error_code": error_code.success
        }
    except Exception as e:
        response = {
            "message": str(e),
            "error_code": error_code.failed
        }
    return jsonify(response)


@users_blueprint.route('/change-password/<int:id>', methods=['PUT'])
@login_required
def change_user_password(id):
    data = request.json
    old_pass = data['old_password']
    new_pass = data['new_password']

    user = Users.query.get(id)

    if not user:
        return jsonify({
            'message': 'Not found',
            'error_code': error_code.not_found
        })

    if not check_password_hash(user.password, old_pass):
        return jsonify({
            'message': 'Old password does not match',
            'error_code': error_code.failed
        })

    user.password = generate_password_hash(new_pass)

    try:
        db.session.commit()
        response = {
            'message': 'Change password successfully',
            'error_code': error_code.success
        }
    except Exception as e:
        db.session.rollback()
        response = {
            'message': str(e),
            'error_code': error_code.failed
        }

    return jsonify(response)
