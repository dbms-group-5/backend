from __future__ import absolute_import

import base64
import io

from flask import Blueprint, jsonify, request, make_response, send_file
from flask_login import login_required

from app.models import db, o2d
from app.models.Posts import Posts
from app.models.Images import Images

from configs.Errors import error_code

posts_blueprint = Blueprint('Posts', __name__)


@posts_blueprint.route("/", methods=['POST'])
@login_required
def create_post():
    data = request.form
    files = request.files
    new_post = Posts(
        title=data['title'],
        description=data['description'],
        content=files['content'].read(),
        author_id=data['author_id']
    )
    try:
        db.session.add(new_post)
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        return jsonify({
            'message': str(e),
            'error_code': error_code.failed
        })

    if files.get('image'):
        new_image = Images(
            image=files['image'].read(),
            post_id=new_post.id
        )

        try:
            db.session.add(new_image)
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            return jsonify({
                'message': str(e),
                'error_code': error_code.failed
            })

    return jsonify({
        'post_id': new_post.id,
        'message': 'created post and content',
        'error_code': error_code.success
    })


@posts_blueprint.route("/", methods=['GET'])
@login_required
def get_all_posts():
    posts = Posts.query.all()
    result = []
    for post in posts:
        p = o2d(post)
        result.append(p)
    response = {
        'data': result
    }
    response['error_code'] = error_code.success
    return jsonify(response)


@posts_blueprint.route("/content/<int:id>", methods=['GET'])
@login_required
def get_content_of_post(id):
    post = Posts.query.get(id)

    if not post:
        return jsonify({
            'message': 'Not found',
            'error_code': error_code.not_found
        })

    content = post.content
    return send_file(io.BytesIO(content), as_attachment=True, mimetype="text/md", attachment_filename='{}_{}.md'.format("content", id))


@posts_blueprint.route("/image/<int:id>", methods=['GET'])
@login_required
def get_image_of_post(id):
    post = Posts.query.get(id)

    if not post:
        return jsonify({
            'message': 'Not found',
            'error_code': error_code.not_found
        })

    content = post.cover_image.image
    return send_file(io.BytesIO(content), as_attachment=True, mimetype="image/jpeg", attachment_filename='{}_{}.jpg'.format("cover_image", id))


@posts_blueprint.route("/<int:id>", methods=['GET'])
@login_required
def get_post(id):
    post = Posts.query.get(id)

    if not post:
        return jsonify({
            'message': 'Not found',
            'error_code': error_code.not_found
        })

    p = {
        'title': post.title,
        'description': post.description,
        'created': post.created.strftime('%Y-%m-%d %H:%M:%S'),
        'author_id': post.author_id,
    }

    response = {
        'data': p
    }
    response['error_code'] = error_code.success

    return jsonify(response)


@posts_blueprint.route("/<int:id>", methods=['PUT'])
@login_required
def update_post(id):
    post = Posts.query.get(id)
    image = Images.query.filter_by(post_id=post.id).first()

    if not post:
        return jsonify({
            'message': 'Not found',
            'error_code': error_code.not_found
        })

    data = request.form
    files = request.files
    post.title = data['title']
    post.description = data['description']
    post.content = files.get('content').read()
    if files.get('image'):
        if not image:
            new_image = Images(
                image=files['image'].read(),
                post_id=post.id
            )
            try:
                db.session.add(new_image)
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                response = {
                    'message': str(e),
                    'error_code': error_code.failed
                }
                return jsonify(response)
        else:
            image.image = files.get('image').read()

    try:
        db.session.commit()
        response = {
            'message': 'Updated post {}'.format(post.id),
            'error_code': error_code.success
        }
    except Exception as e:
        db.session.rollback()
        response = {
            'message': str(e),
            'error_code': error_code.failed
        }

    return jsonify(response)


@posts_blueprint.route('/<int:id>', methods=['DELETE'])
@login_required
def delete_post_by_id(id):
    post = Posts.query.get(id)
    if not post:
        return jsonify({
            'message': 'Not found',
            'error_code': error_code.not_found
        })
    image = Images.query.filter_by(post_id=post.id).first()
    if image:
        try:
            db.session.delete(image)
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            response = {
                'message': str(e),
                'error_code': error_code.failed
            }
            return jsonify(response)

    try:
        db.session.delete(post)
        db.session.commit()
        response = {
            'message': 'Deleted post {}'.format(id),
            'error_code': error_code.success
        }
    except Exception as e:
        db.session.rollback()
        response = {
            'message': str(e),
            'error_code': error_code.failed
        }

    return jsonify(response)
