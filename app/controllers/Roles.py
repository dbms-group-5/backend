from __future__ import absolute_import

from flask import Blueprint, jsonify, request

from app.models.Roles import Roles
from app.models import db, o2d

from configs.Errors import error_code

role_blueprint = Blueprint('Roles', __name__)


@role_blueprint.route("/", methods=['GET'])
def get_all_roles():
    roles = Roles.query.all()
    response = []
    for role in roles:
        response.append(o2d(role))

    return jsonify({
        'results': response,
        'error_code': error_code.success
    })


@role_blueprint.route('/', methods=['POST'])
def create_role():
    name = request.json["name"]

    if not name:
        return jsonify({
            "message": "Missing fields",
            "error_code": error_code.missing_field
        })

    try:
        new_role = Roles(name=name)
        db.session.add(new_role)
        db.session.commit()
        response = o2d(new_role)
        response["error_code"] = error_code.success
    except Exception as e:
        db.session.rollback()
        response = {
            "message": str(e),
            "error_code": error_code.failed
        }

    return jsonify(response)


@role_blueprint.route("/<int:id>", methods=["DELETE"])
def delete_role(id):
    role = Roles.query.get(id)

    if not role:
        return jsonify({
            "message": "Not found",
            "error_code": error_code.not_found
        })

    try:
        db.session.delete(role)
        db.session.commit()
        response = {
            "message": "Deleted role {}".format(role.name),
            "error_code": error_code.success
        }
    except Exception as e:
        db.session.rollback()
        response = {
            "message": str(e),
            "error_code": error_code.failed
        }

    return jsonify(response)


@role_blueprint.route("/<int:id>", methods=["PUT"])
def update_role(id):
    role = Roles.query.get(id)

    name = request.json["name"]

    if not role:
        return jsonify({
            "message": "Not found",
            "error_code": error_code.not_found
        })

    role.name = name

    try:
        db.session.commit()
        response = {
            "message": "Updated role {}".format(role.name),
            "error_code": error_code.success
        }
    except Exception as e:
        db.session.rollback()
        response = {
            "message": str(e),
            "error_code": error_code.failed
        }

    return jsonify(response)
