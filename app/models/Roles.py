from sqlalchemy_serializer import SerializerMixin
from app.models import db


class Roles(db.Model, SerializerMixin):
    __tablename__ = 'roles'

    id = db.Column(db.Integer, db.Sequence(
        __tablename__ + '_id_seq'), primary_key=True)
    name = db.Column(db.String(64), nullable=False, unique=True)

    serialize_rules = ('-users.role',)

    users = db.relationship('Users', backref='role')

    def __init__(self, name):
        self.name = name
