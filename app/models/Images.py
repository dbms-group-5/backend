from sqlalchemy_serializer import SerializerMixin
from app.models import db
from datetime import datetime


class Images(db.Model, SerializerMixin):
    __tablename__ = "images"

    id = db.Column(db.Integer, db.Sequence(
        __tablename__ + '_id_seq'), primary_key=True)
    image = db.Column(db.LargeBinary, nullable=False)
    created = db.Column(db.DateTime, nullable=False)

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    post_id = db.Column(db.Integer, db.ForeignKey('posts.id'))

    def __init__(self, image, user_id=None, post_id=None):
        self.image = image
        self.user_id = user_id
        self.post_id = post_id
        self.created = datetime.now()
