from sqlalchemy_serializer import SerializerMixin
from app.models import db
from datetime import datetime


class Posts(db.Model, SerializerMixin):
    __tablename__ = "posts"

    id = db.Column(db.Integer, db.Sequence(
        __tablename__ + '_id_seq'), primary_key=True)
    title = db.Column(db.String(128), nullable=False)
    description = db.Column(db.Text, nullable=False)
    content = db.Column(db.LargeBinary, nullable=False)
    created = db.Column(db.DateTime, nullable=False)

    serialize_rules = ('-cover_image.post')

    cover_image = db.relationship('Images', uselist=False, backref='post')

    author_id = db.Column(
        db.Integer, db.ForeignKey('users.id'), nullable=False)

    def __init__(self, title, description, content, author_id):
        self.title = title
        self.description = description
        self.content = content
        self.author_id = author_id
        self.created = datetime.now()
