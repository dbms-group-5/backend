from sqlalchemy_serializer import SerializerMixin
from app.models import db
from flask_login import UserMixin
from datetime import datetime


class Users(UserMixin, db.Model, SerializerMixin):
    __tablename__ = 'users'

    id = db.Column(db.Integer, db.Sequence(
        __tablename__ + '_id_seq'), primary_key=True)
    username = db.Column(db.String(64), unique=True, nullable=False)
    password = db.Column(db.String(256), nullable=False)
    name = db.Column(db.String(80), nullable=False)
    birthday = db.Column(db.Date, nullable=True)
    phone = db.Column(db.String(10), nullable=True)
    email = db.Column(db.String(128), nullable=False, unique=True)
    gender = db.Column(db.String(6), nullable=False)
    created = db.Column(db.DateTime, nullable=False)

    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'), nullable=False)

    serialize_rules = ('-image.user', '-posts.user')

    image = db.relationship('Images', uselist=False, backref='user')
    posts = db.relationship('Posts', backref='user')

    def __init__(self, username, password, name, birthday, phone, email, gender,
                 role_id):
        self.username = username
        self.password = password
        self.name = name
        self.birthday = birthday
        self.phone = phone
        self.email = email
        self.gender = gender
        self.role_id = role_id
        self.created = datetime.now()
