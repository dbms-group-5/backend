from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def o2d(ob):
    d = {}
    for column in ob.__table__.columns:
        d[column.name] = str(getattr(ob, column.name))
    return d
