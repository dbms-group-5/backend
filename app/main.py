from __future__ import absolute_import

from flask import Flask
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_cors import CORS

from app.models.Users import Users

from app.controllers.Users import users_blueprint
from app.controllers.Posts import posts_blueprint
from app.controllers.Images import image_blueprint
from app.controllers.Roles import role_blueprint

from app.models import db

from configs.Config import Config

app = Flask(__name__)
CORS(app, supports_credentials=True, resources={r"/*": {"origins": "*"}})

app.config.from_object(Config)
db.init_app(app)

login_manager = LoginManager()
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    # since the user_id is just the primary key of our user table, use it in the query for the user
    return Users.query.get(int(user_id))


app.register_blueprint(users_blueprint, url_prefix="/users")
app.register_blueprint(posts_blueprint, url_prefix="/posts")
app.register_blueprint(image_blueprint, url_prefix='/images')
app.register_blueprint(role_blueprint, url_prefix='/roles')

migrate = Migrate(app, db)
