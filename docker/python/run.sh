#!/usr/bin/env bash
if [ ! -f "./venv/activate" ]; then
    python3 -m pip install virtualenv
    virtualenv venv
fi
source ./venv/bin/activate
python3 -m pip install -r requirements.txt
export LC_ALL=en_US.utf-8
export LANG=en_US.utf-8
/usr/local/docker-entrypoint.sh
flask db init
flask db migrate
flask db upgrade
python3 manage.py initialize
flask run --host=0.0.0.0