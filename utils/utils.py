from __future__ import absolute_import

from datetime import datetime


def parseDateTime(string):
    return datetime.strptime(string, "%m/%d/%Y %H:%M:%S")


def parseDate(string):
    return datetime.strptime(string, "%m/%d/%Y")
