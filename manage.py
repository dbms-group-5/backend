from app.main import app
from app.models import db

from app.models.Users import Users
from app.models.Roles import Roles

from configs.Default import roles

from flask_script import Manager

from werkzeug.security import generate_password_hash
from datetime import datetime

manage = Manager(app)


def create_roles():
    for role in roles:
        try:
            new_role = Roles(name=role)
            db.session.add(new_role)
            db.session.commit()
            print("Created role {}".format(role))
        except Exception as e:
            db.session.rollback()
            raise Exception(str(e))


@manage.command
def initialize():
    create_roles()

    admin_username = app.config["ADMIN_USERNAME"]
    admin_password = generate_password_hash(app.config["ADMIN_PASSWORD"])
    admin_email = app.config["ADMIN_EMAIL"]
    admin_role = Roles.query.filter_by(name=roles[0]).first()

    admin = Users(
        username=admin_username,
        password=admin_password,
        name="Administrator",
        birthday=datetime.now(),
        phone="0123456789",
        email=admin_email,
        gender="none",
        role_id=admin_role.id
    )

    try:
        db.session.add(admin)
        db.session.commit()
        print("Created admin")
    except Exception as e:
        db.session.rollback()
        raise Exception(str(e))


if __name__ == "__main__":
    manage.run()
