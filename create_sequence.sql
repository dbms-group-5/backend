create sequence roles_id_seq start with 1 increment by 1 nocache nocycle;
create sequence users_id_seq start with 1 increment by 1 nocache nocycle;
create sequence images_id_seq start with 1 increment by 1 nocache nocycle;
create sequence posts_id_seq start with 1 increment by 1 nocache nocycle;